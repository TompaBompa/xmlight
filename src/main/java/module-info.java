module xmlight {
    
    requires transitive java.sql;
    requires transitive java.xml;
    
    exports xmlight;
    exports xmlight.format;
    exports xmlight.transform;
}
