/*
 * XmlPrettyPrinter.java
 *
 * Created on den 5 oktober 2007, 13:28
 *
 */
package xmlight.format;

import java.io.StringWriter;
import java.nio.CharBuffer;
import xmlight.XmlDocument;
import xmlight.XmlNode;

/**
 * Used to format an XML output string, to make it human readable.
 * @author Thomas Boqvist, Digit Man
 */
public class XmlPrettyPrinter {

    private int tabsize = 2;
    private boolean endTag = false;
    private String xmlData;

    /**
     * Creates an XmlPrettyPrinter from a String object
     * @param xmlData The XML to format as a String
     */
    public XmlPrettyPrinter(String xmlData) {
        this.xmlData = xmlData;
    }

    /**
     * Creates an XmlPrettyPrinter from an XmlNode
     * @param xml The root node object to print
     */
    public XmlPrettyPrinter(XmlNode xml) {
        this(xml.toString());
    }

    /**
     * Creates an XmlPrettyPrinter from an XmlNode
     * @param xml The XmlDocument object to print
     */
    public XmlPrettyPrinter(XmlDocument xml) {
        this(xml.toString());
    }

    private String newline(int tab) {
        return "\n" + CharBuffer.allocate(tab).toString().replace('\0', ' ');
    }

    /**
     * Performs the actual processing of the contained XML data.
     * @return The formatted XML result as a String
     */
    public String process() {
        char prevChar = ' ',  nextChar = ' ',  token = ' ';
        int tab = -2;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < xmlData.length(); i++) {
            token = xmlData.charAt(i);
            if (i < xmlData.length() - 1) {
                nextChar = xmlData.charAt(i + 1);
            }
            if (token == '<') {
                if (nextChar != '/' && nextChar != '?') {
                    tab += getTabsize();
                }
                if (prevChar == '>' && nextChar != '/' || endTag) {
                    builder.append(newline(tab));
                }
                if (nextChar == '/') {
                    tab -= getTabsize();
                }
                endTag = false;
            } else if (token == '/') {
                if (prevChar == '<' || nextChar == '>') {
                    endTag = true;
                    if (nextChar == '>') {
                        tab -= getTabsize();
                    }
                }
            }
            prevChar = token;
            builder.append(token);
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return process();
    }

    /**
     * Tabsize getter
     * @return The tab size used when formatting
     */
    public int getTabsize() {
        return tabsize;
    }

    /**
     * Tabsize setter
     * @param tabsize The tab size to use when formatting the output
     */
    public void setTabsize(int tabsize) {
        this.tabsize = tabsize;
    }
}
