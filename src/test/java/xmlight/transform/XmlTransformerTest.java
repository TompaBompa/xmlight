package xmlight.transform;

import javax.xml.transform.stream.StreamSource;
import org.junit.Before;
import org.junit.Test;
import xmlight.DocumentToXmlNodeParser;
import xmlight.XmlNode;
import static org.junit.Assert.*;

/**
 *
 * @author thomas
 */
public class XmlTransformerTest {

    private DocumentToXmlNodeParser parser;
    private XmlTransformer transformer;
    XmlNode sourceXml;

    @Before
    public void setUp() {
        sourceXml = new DocumentToXmlNodeParser(ClassLoader.getSystemResourceAsStream("test-source.xml")).parse();
    }

    @Test
    public void testProcessWithFailingContext() {
        transformer = new XmlTransformer(sourceXml, new StreamSource(ClassLoader.getSystemResourceAsStream("test-stylesheet2.xsl")));
        try {
            transformer.process();
            fail("Exception must be thrown");
        } catch (Exception ex) {
        }
    }

    @Test
    public void testProcessWithClasspathContext() {
        transformer = new XmlTransformer(sourceXml, new StreamSource(ClassLoader.getSystemResourceAsStream("test-stylesheet.xsl")));
        transformer.setUriResolver(new ClasspathResolver("se/digitman/lightxml/transform/include", getClass().getClassLoader()));
        XmlNode result = transformer.process();
        assertEquals("html", result.getName());
        assertEquals("text1", result.getXmlNodeByPath("head/title").getText());
        assertEquals("1", result.getXmlNodeByPath("body/h4").getText());
    }
}
